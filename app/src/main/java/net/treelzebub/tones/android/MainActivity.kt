package net.treelzebub.tones.android

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import co.touchlab.kermit.Logger
import net.treelzebub.tones.android.ui.dragdrop.LazyColumnDragAndDrop
import net.treelzebub.tones.android.ui.theme.TonesTheme
import net.treelzebub.tones.injectLogger
import net.treelzebub.tones.models.BreedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent

class MainActivity : ComponentActivity(), KoinComponent {

    private val log: Logger by injectLogger("MainActivity")
    private val viewModel: BreedViewModel by viewModel()

    private val testList = List(50) { it }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TonesTheme {
                LazyColumnDragAndDrop(testList)
            }
        }
    }
}
