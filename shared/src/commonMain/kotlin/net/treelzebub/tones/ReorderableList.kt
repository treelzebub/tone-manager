package net.treelzebub.tones

import co.touchlab.kermit.Logger
import co.touchlab.kermit.LoggerConfig

fun <T> reorderableListOf(vararg elements: T): ReorderableList<T> {
    val list = if (elements.size > 0) elements.asList() else emptyList()
    return ReorderableList(list)
}

class ReorderableList<T>(init: List<T> = emptyList()) {
    private val log: Logger = Logger(LoggerConfig.default, "ReorderableList")

    var list: List<T> = init
        private set

    operator fun get(index: Int) = list[index]

    fun add(element: T) {
        val copy = copy()
        copy.add(element)
        list = copy
    }

    fun add(index: Int, element: T) {
        val copy = copy()
        copy.add(index, element)
        list = copy
    }

    fun remove(index: Int) {
        val copy = copy()
        copy.removeAt(index)
        list = copy
    }

    fun remove(element: T) {
        remove(list.indexOf(element))
    }

    fun move(indexToMove: Int, destination: Int) {
        val copy = copy()
        val movedObject = copy[indexToMove]
        copy[indexToMove] = copy[destination]
        copy[destination] = movedObject
        log.d("Moved $indexToMove to $destination. \nPrev: $list\nNew: $copy")
        list = copy
    }

    fun replace(index: Int, element: T) {
        val copy = copy()
        copy[index] = element
        list = copy
    }

    private fun copy(): MutableList<T> = ArrayList(list)
}