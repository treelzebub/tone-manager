package net.treelzebub.tones.ktor

import net.treelzebub.tones.response.BreedResult

interface DogApi {
    suspend fun getJsonFromApi(): BreedResult
}
