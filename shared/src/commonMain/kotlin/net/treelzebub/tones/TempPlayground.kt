package net.treelzebub.tones

import net.treelzebub.tones.models.SignalMod
import net.treelzebub.tones.models.Potentiometer
import net.treelzebub.tones.models.Switch
import net.treelzebub.tones.models.buildSignalMod

val signalMod: SignalMod = buildSignalMod {
    name = "Jekyll & Hyde"

    potentiometers = listOf(
        Potentiometer(
            name = "Volume",
            description = "",
            range = 0..10,
            increment = Potentiometer.Increment.Float),
        Potentiometer(
            name = "Tone",
            description = "",
            range = 0..10,
            increment = Potentiometer.Increment.Float
        )
    )

    switches = listOf(
        Switch(
            name = "Overdrive",
            type = Switch.Type.Footswitch(),
            position = 1
        ),
        Switch(
            name = "Bass Boost",
            type = Switch.Type.A_B(),
            position = 1
        )
    )
}
