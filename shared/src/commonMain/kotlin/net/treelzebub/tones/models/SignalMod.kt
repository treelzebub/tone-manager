package net.treelzebub.tones.models

fun buildSignalMod(fn: SignalMod.Builder.() -> Unit): SignalMod = SignalMod.Builder().apply(fn).build()

/**
 * TODO
 * TODO * Add optional pedal description and "local" tone description
 * TODO *
 * TODO
 */
data class SignalMod(
    val name: String,
    val type: Type,
    val description: String,
    val potentiometers: List<Potentiometer>,
    val switches: List<Switch>,
) {

    enum class Type {
        Pedal, Amplifier;
    }

    class Builder(
        var name: String = "",
        var type: Type = Type.Pedal,
        var description: String = "",
        var potentiometers: List<Potentiometer> = listOf(),
        var switches: List<Switch> = listOf(),
        var number: Int = 0
    ) {
        constructor(existing: SignalMod) : this(
            existing.name, existing.type, existing.description, existing.potentiometers, existing.switches
        )

        fun build() = SignalMod(this.name, this.type, this.description, this.potentiometers, this.switches)
    }

    fun copy(fn: Builder.() -> Unit) = Builder(this).apply(fn).build()
}

data class Potentiometer(
    val name: String,
    val description: String,
    val range: IntRange,
    val increment: Increment
) {

    enum class Increment {
        Int, Float;
    }
}

data class Switch(
    val name: String,
    val type: Type,
    val position: Int
) {

    sealed class Type(val settings: Int) {

        class Footswitch : Type(2)

        class A_B : Type(2)

        class A_B_A_Plus_B : Type(3)

        class Custom(settings: Int) : Type(settings)
    }
}
