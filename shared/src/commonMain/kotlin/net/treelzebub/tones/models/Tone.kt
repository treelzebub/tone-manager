package net.treelzebub.tones.models

import net.treelzebub.tones.ReorderableList

fun buildTone(fn: Tone.Builder.() -> Unit) = Tone.Builder().apply(fn).build()

data class Tone(
    val name: String,
    val description: String,
    val signalChain: ReorderableList<SignalMod>
) {

    class Builder(
        var name: String = "",
        var description: String = "",
        var signalChain: ReorderableList<SignalMod> = ReorderableList()
    ) {

        constructor(existing: Tone) : this(
            existing.name, existing.description, existing.signalChain
        )

        fun build() = Tone(this.name, this.description, this.signalChain)
    }

    fun copy(fn: Builder.() -> Unit) = Builder(this).apply(fn).build()
}