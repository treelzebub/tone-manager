package net.treelzebub.tones

import com.squareup.sqldelight.db.SqlDriver

internal expect fun testDbConnection(): SqlDriver
