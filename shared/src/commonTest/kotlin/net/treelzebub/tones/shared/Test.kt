package net.treelzebub.tones.shared

import net.treelzebub.tones.ReorderableList
import net.treelzebub.tones.signalMod
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class Test {

    @Test
    fun bounds() {
        val list = ReorderableList(listOf(1, 2, 3))
        list.move(2, 1)
        assertEquals(listOf(1, 3, 2), list.list)
        assertFails { list.move(3, 2) }
    }

    @Test
    fun move() {
        val list1 = ReorderableList(listOf(1, 2, 3))
        val copy1 = list1.list
        list1.move(2, 2)
        assertTrue { list1.list == copy1 }

        val list2 = ReorderableList(listOf(1, 2, 3))
        list2.move(2, 1)
        assertTrue(list2.list.toString()) { listOf(1, 3, 2) == list2.list }
    }

    @Test
    fun replace() {
        val list = ReorderableList(listOf(1, 4, 9, 16, 25))
        list.replace(3, 0)
        assertTrue { listOf(1, 4, 9, 0, 25) == list.list }
    }

    @Test
    fun pedal() {
        assertEquals(2, signalMod.switches.size)
    }
}